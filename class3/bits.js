const MAX_FIELDS = [
  {name: 'localId', value: 10000},
  {name: 'productId', value: 1000},
  {name: 'quantity', value: 1000},
  {name: 'days', value: 365},
  {name: 'year', value: 99},
  {name: 'time', value: 3600}
]

function leftpad(s, length, fill) {
  if (!s) return null
  if (length < s.length) return s
  fill = fill || ' '
  let r = '' + s
  while (r.length < length) {
    r = fill + r
  }
  return r
}

function rightpad(s, length, fill) {
  if (!s) return null
  if (length < s.length) return s
  fill = fill || ' '
  let r = '' + s
  while (r.length < length) {
    r += fill
  }
  return r
}

function binaryString(value, length) {
  const binary = value.toString(2)
  if (binary.length > length) {
    throw new Error('value ' + value + ' out of range')
  }
  return leftpad(binary, length, '0')
}

function checksum(str) {
  return str
    .split('')
    .map(c => c.charCodeAt(0))
    .reduce((acc, num) => acc ^ num)
    .toString(16)
}

function encode(fields) {
  const bits = MAX_FIELDS
    .map(max => {
      const maxLength = max.value.toString(2).length
      return binaryString(fields[max.name], maxLength)
    })

  const bytes = []
  let acc = ''

  bits.join('').split('').forEach(bit => {
    acc += bit
    if (acc.length == 8) {
      bytes.push(acc)
      acc = ''
    }
  })

  if (acc.length) {
    bytes.push(rightpad(acc, 8, '0'))
  }

  const result = bytes
    .map(b => String.fromCharCode(parseInt(b, 2)))
    .join('')

  //TODO: send checksum
  //return `${result}*${checksum(result)}`
  return result
}

function decode(bytes) {
  //TODO: remove checksum and checkit later
  const bits = bytes
    .split('')
    .map(c => leftpad(c.charCodeAt(0).toString(2), 8, '0'))
    .join('').split('')

  const maxLengths = MAX_FIELDS.map(max => {
    return {name: max.name, length: max.value.toString(2).length}
  })

  const values = {}
  let max
  let maxIndex = 0
  let acc = ''
  bits.forEach(bit => {
    if (maxIndex >= maxLengths.length) {
      // discard bits at the end
      return
    }

    max = maxLengths[maxIndex]
    acc += bit
    if (acc.length == max.length) {
      values[max.name] = parseInt(acc, 2)
      maxIndex++
      acc = ''
    }
  })

  return values
}

const object = {
  localId: 1337,
  productId: 123,
  quantity: 5,
  days: 42,
  year: 17,
  time: 1800
}

console.log('object:', object)
console.log('encode(object):', encode(object))
console.log('decode(encode(object)):', decode(encode(object)))

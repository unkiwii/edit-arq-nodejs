const fs = require('fs')

const guidRx = /^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12};.+/gm

const heads = [
  'guid',
  'code',
  'description',
  'price',
  'minStock',
  'tax',
  'typeCode',
  'typeDescription',
  'manufacturerCode',
  'manufacturerDescription',
  'familyCode',
  'familyDescription',
  'isPharmacy',
  'isOther'
]

const items = fs.readFileSync('data/items2.csv', 'utf8')

const products = items.match(guidRx)
  .map(line => line.split(';'))
  .filter(line => line.length === 14)
  .reduce((products, line) => {
    products[line[0]] = line.reduce((product, value, index) => {
      product[heads[index]] = value
      return product
    }, {})
    return products
  }, {})

return products

const http = require('http')
const express = require('express')
const app = express()

const prices = require('./prices')

app.get('/prices/:clientId', (req, res) => {
  const clientId = req.params.clientId
  res.send(clientId)
})

app.listen(8000, () => {
  console.log('listeing in 8000')
})

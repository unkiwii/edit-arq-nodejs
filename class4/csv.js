const fs = require('fs')

const encoding = 'utf8'
const guidRx = /^([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12});/m

const contents = [
  {
    name: 'clients',
    header: 'data/heads_entiedades_clientes2.csv',
    content: 'data/entidades_clientes2.csv'
  },
  {
    name: 'items',
    header: 'data/heads_items2.csv',
    content: 'data/items2.csv'
  }
]

const data = contents.reduce((acc, c) => {
  let data = {}

  //try {
    const headers = fs.readFileSync(c.header, encoding)
      .split(',')   // headers well separated
      .map(h => h.trim().toLowerCase())

    let show = true
    const csv = fs.readFileSync(c.content, encoding)
      .split(guidRx)   // csv not well separated
      .reduce((prev, curr, index) => {
        if (index == 0) return prev
        if (index % 2 == 1) prev.push([curr])
        if (index % 2 == 0) {
          console.log('k',curr)
          if (!show) {
            throw new Error()
          }
          show = false
          prev[prev.length - 1] = prev[prev.length - 1].concat(
            curr.split(';').map(c => c.replace('\r\n', '').trim())
          )
        }
        return prev
      }, [])

    console.log(csv.slice(0, 2))

    throw new Error()

    const headCount = headers.length
    data = csv.reduce((prev, d, index) => {
      index = index % headCount
      const head = headers[index]
      prev[head] = prev[head] || []
      prev[head].push(d)
      return prev
    }, {})
  //} catch (e) {
    //data = e
  //}

  acc[c.name] = data

  return acc
}, {})

console.log(data)


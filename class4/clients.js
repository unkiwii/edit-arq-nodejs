const fs = require('fs')

const guidRx = /^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12};.+/gm

const heads = [
  'Guid',
  'GuidEntidad',
  'GuidGrupoEmpresa',
  'IdEmpresa',
  'Codigo',
  'GuidPrecioLista',
  'LimiteDeCredito',
  'MensajeCliente',
  'Observaciones',
  'ObservCtaCte',
  'Descripcion',
  'Contacto',
  'Comprador',
  'Vendedor',
  'Gerente',
  'Tesorero',
  'Activo',
  'DiasDeAtraso',
  'IdDeudaTipo',
  'MontoVencido',
  'MontoCtaCte',
  'Cheques',
  'SaldoDisponible',
  'MontoChequesCancelarMes',
  'FechaProxPago',
  'Documentos',
  'PedSinFacturar',
  'Cheques30_60',
  'ChequesMay60',
  'GuidClienteTipo',
  'ClienteTipo'
]

const data = fs.readFileSync('data/entidades_clientes2.csv', 'utf8')

const clients = data.match(guidRx)
  .map(line => line.split(';'))
  .filter(line => line.length === heads.length)
  .reduce((clients, line) => {
    clients[line[0]] = line.reduce((client, value, index) => {
      client[heads[index]] = value
      return client
    }, {})
    return clients
  }, {})

return clients

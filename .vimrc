" vim:foldmethod=marker:expandtab:

syntax on
filetype plugin indent on

set nocompatible
set encoding=utf8
set fileencoding=utf8
set termencoding=utf8
set autowrite
set autoread
set modeline
set modelines=5
set nowritebackup
set nobackup
set noswapfile
set hlsearch
set incsearch
set backspace=indent,eol,start
set autoindent
set smartindent
set showcmd
set showmatch
set showmode
set nowrap
set textwidth=0
set splitright    " open new buffer on the right of the current one (with vsplit)
set splitbelow    " open new buffer below the current one (with split)
set wildmenu
set wildmode=longest,full
set number
set path=.,**
set exrc
set laststatus=2 "show status bar always
set ruler
set rulerformat=%30(%=%y\ %l,%c\ %P%)
set ts=2 sts=2 sw=2 expandtab smarttab
set list
set listchars=eol:$,tab:\|\ ,trail:-
nnoremap j gj
nnoremap k gk
vnoremap < <gv
vnoremap > >gv
nnoremap <silent> ; :nohlsearch<cr>
vnoremap <silent> ; <esc>:nohlsearch<cr>
nnoremap <silent> <s-s> a<cr><esc>

const providers = require('./providers')
const products = require('./products')

module.exports = Application => ({
  providers: providers(Application),
  products: products(Application)
})

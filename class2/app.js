const http = require('http')
const mongojs = require('mongojs')
const express = require('express')

const Mocks = require('./mocks')
const Middleware = require('./middleware')

const collections = [
  'providers', 'products', 'modules', 'aditionals'
]

const db = mongojs('mongodb://127.0.0.1/insurances', collections)

const app = express()
const server = http.createServer(app)

const Application = {
  app,
  db,
}

Application.mocks = Mocks(Application)
Application.middleware = Middleware(Application)

app.get('/makeproducts', Application.mocks.products)
app.get('/makeproviders', Application.mocks.providers)

collections.forEach(collection =>
  ['get', 'put', 'patch', 'delete'].forEach(method =>
    app[method](`/${collection}`, Application.middleware.universal)
  )
)

app.listen(3000, () => {
  console.log('Listening on port 3000')
})

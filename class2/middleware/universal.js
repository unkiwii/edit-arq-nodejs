const universal = ({ db }) => (req, res) => {
  const collection = req.url.slice(1)
  const method = req.method.toLowerCase()
  switch (method) {
    case 'get':
      return find(db, collection, res)
    default:
      res.json({err: 'unknown method ' + method})
  }
}

const find = (db, collection, res) => {
  db[collection].find({}, {}, (err, data) => {
    if (err) return res.json({err})
    else return res.json(data)
  })
}

module.exports = universal

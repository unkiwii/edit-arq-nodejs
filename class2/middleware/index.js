const universal = require('./universal')

module.exports = Application => ({
  universal: universal(Application)
})
